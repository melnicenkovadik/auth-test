import React from 'react';
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import {makeStyles} from "@material-ui/core/styles";
import MergeTypeIcon from '@material-ui/icons/MergeType';
import FormLogin from "./Form";
import {Footer} from "./Footer";


const useStyles = makeStyles((theme) => ({
    paper: {
        height: "92%",
        position: "relative",
        margin: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: "flex-start",
        justifyContent: "flex-start"
    },

    headerLogo: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        textTransform: "uppercase",
        color: '#22252C',
        fontWeight: 600,
        marginBottom: 119,
    },
    singInText: {
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: 28,
        color: "#333333",
    },
    text: {
        marginTop: 15,
        fontWeight: 600,
        fontSize: 14,
        color: '#333333'
    },
    link: {
        cursor:"pointer",
        fontStyle: "normal",
        fontWeight: 600,
        fontSize: 14,
        color: "#065BEA",
        marginBottom: 61,
        textDecoration: "none",
        "&:hover": {
            color: '#0A4AB6',
            textDecoration: "none",

        }
    },

}));

export default function LoginForm() {
    const classes = useStyles();

    return (
        <>
            <div
                className={classes.paper}>
                <Typography
                    className={classes.headerLogo}>
                    <MergeTypeIcon
                        fontSize={'large'}/>
                    <span>Merge development</span>
                </Typography>
                <Typography
                    className={classes.singInText}
                    component="h1"
                    variant="h5">
                    Sign in
                </Typography>
                <Typography
                    className={classes.text}>
                    Don’t have an account?
                    <Link className={classes.link}>Sign up
                        now</Link>
                </Typography>
                <FormLogin/>

                <Footer/>
            </div>
        </>
    );
}

