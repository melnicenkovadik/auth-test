import Link from "@material-ui/core/Link";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        color: "#75777A",
        marginTop: 30,
        position: "absolute",
        bottom: 0,
        left: 0,
    },
    link: {
        fontSize: 14,
        fontWeight: 600,
        marginRight: 24,
        color: "#75777A",
        "&:hover": {
            color: "#333333",
            textDecoration: "none"

        }
    }
}))

const links = [
    {
        id: 1,
        title: 'Contact',
        component: "#"
    },
    {
        id: 2,
        title: 'Privacy',
        component: "#"
    },
    {
        id: 3,
        title: 'Terms',
        component: "#"
    },
]
export const Footer = () => {
    const classes = useStyles();

    return (
        <div
            className={classes.root}>
            {
                links.map((l, i) => (
                    <Link
                        className={classes.link}
                        key={l.id}
                        color="inherit"
                        href={l.component}>
                        {l.title}
                    </Link>
                ))
            }
        </div>
    );
}