import React, {useEffect, useState} from 'react';
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import {makeStyles} from "@material-ui/core/styles";
import {Button} from "@material-ui/core";
import {useFormik} from 'formik';
import * as yup from 'yup';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
        form: {
            width: '100%',
            marginTop: 60,
        },
        label: {
            marginTop: 15,
            fontWeight: 600,
            fontSize: 14,
            color: '#333333'
        },
        submit: {
            marginTop: 10,
            boxShadow: "0px 16px 29px -13px rgba(6,91,234,0.71)",
            color: "white",
            fontSize: 14,
            textTransform: "None",
            margin: theme.spacing(3, 0, 2),
            background: "#065BEA",
            borderRadius: 8,
            width: "100%",
            height: 54,
            "&:hover": {
                background: "#0451D2",

            }
        },
        clearFix: {
            position: "relative"
        },

        color: {
            position: "absolute",
            top: 34,
            right: 10,
            fontSize: 14,
            color: "#75777A",
            "&:hover": {
                color: "#333333",
                textDecoration: "none"
            }
        },
        successInput: {
            border: "1px solid #2eff00",
            borderRadius: 5,
        },
        unSuccessInput: {
            border: "none",

        }
    }))
;
export default function FormLogin() {
    const classes = useStyles();
    const [authorizationData, setAuthorizationData] = useState(false)

    const validationSchema = yup.object({
        email: yup
            .string('Enter your email')
            .email('Invalid email')
            .required('Email is required'),
        password: yup
            .string('Enter your password')
            .min(8, 'Invalid format too short')
            .required('Password is required'),
    });
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            try {
                console.log(JSON.stringify(values, null, 2))
                setAuthorizationData(prevState => !prevState)
                setTimeout(() => {
                    setAuthorizationData(prevState => !prevState)

                }, 2000)
            } catch (e) {
                throw e
            } finally {
                console.log('Finally')
            }

        },
    });
    useEffect(() => {
        console.log(authorizationData);
    }, [authorizationData])
    return (
        <>
            <form
                onSubmit={formik.handleSubmit}
                className={classes.form}
            >
                <label
                    className={classes.label}>Email</label>
                <TextField
                    className={formik.values.email.length > 0 && !formik.errors.email ? classes.successInput : ''}
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    name="email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    helperText={formik.touched.email && formik.errors.email}
                />
                <label
                    className={classes.label}>Password</label>
                <div className={classes.clearFix}>
                    <TextField
                        className={formik.values.password.length > 0 && !formik.errors.password ? classes.successInput : ''}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        type="password"
                        id="password"
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        error={formik.touched.password && Boolean(formik.errors.password)}
                        helperText={formik.touched.password && formik.errors.password}
                    />
                    <Link
                        className={classes.color}
                        href="#"
                        variant="body2">
                        {"Forgot your password?"}
                    </Link>
                </div>

                <Button
                    disabled={!formik.values.email || !formik.values.password}
                    type="submit"
                    fullWidth
                    variant="contained"
                    className={classes.submit}
                >
                    Sign in
                </Button>

            </form>
            {authorizationData ?
                (<Alert severity="success">
                    Мелочь, а приятно)
                </Alert>) :
                null}
        </>
    );
}


