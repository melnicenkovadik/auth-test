import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from '@material-ui/core/styles';
import SliderComponent from "../Slider";
import LoginForm from "../LoginContainer";
//material hook for resize
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useStyles = makeStyles(() => ({
    root: {
        fontFamily: 'Montserrat',
        padding: 20,
        minHeight: "760px"
    },
    imageContainer: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        paddingTop: 100,
        paddingRight: 100,
        paddingLeft: 100,
        borderRadius: 10,
        backgroundColor: "#0656DC",
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    },
    centered: {
        maxWidth: 512
    }

}));

export const App = () => {
    const matches = useMediaQuery('(min-width:600px)');

    console.log(matches);
    const classes = useStyles();

    return (
        //////Two containers//////
        <Grid
            container
            className={classes.root}>
            <CssBaseline/>
            {
                matches ?
                    <Grid
                        minWidth={900 ? {display: 'none'} : ''}
                        item
                        xs={12}
                        sm={"auto"}
                        md={7}
                        className={classes.imageContainer}>
                        <div
                            className={classes.centered}>
                            <SliderComponent/>
                        </div>
                    </Grid> : ''
            }

            <Grid
                item
                xs={12}
                sm={8}
                md={5}
                elevation={6}>
                <LoginForm/>
            </Grid>
        </Grid>
    );
}