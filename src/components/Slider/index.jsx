import React from "react";
import Slider from "react-slick";
import img1 from "../../assets/slides/img.png"
import img2 from "../../assets/slides/img_1.png"
import img3 from "../../assets/slides/img_2.png"
import img4 from "../../assets/slides/img_3.png"
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const useStyles = makeStyles(() => ({
    title: {
        fontWeight: 600,
        fontSize: 20,
        textAlign: 'center',
        color: '#fff'

    },
    description: {
        marginTop: 10,
        textAlign: 'center',
        fontWeight: 600,
        fontSize: 14,
        color: '#fff'

    },
    image: {
        width: "100%",
        maxWidth: "100%",
        height: "332px",
    },

}));
const images = [
    {
        id: 1,
        image: img1,
        title: "Tokenplace",
        description: "Multi-exchange Trading Terminal"
    },
    {
        id: 2,
        image: img2,
        title: "Merge Data Review",
        description: "It gives data managers the power to efficiently manage quality data to ensure database lock readiness."
    },
    {
        id: 3,
        image: img3,
        title: "Fund Platform",
        description: "Hedge funds wealth management"
    },
    {
        id: 4,
        image: img4,
        title: "Noviscient",
        description: "Management Platform"
    },
]
export default function SliderComponent() {
    const classes = useStyles();

    function ArrowDisplayNone() {
        return (
            <div
                style={{
                    display: "none",
                }}
            />
        );
    }

    let settings = {
        className: "center",
        infinite: true,
        slidesToShow: 1,
        dots: true,
        autoplay: true,
        pauseOnHover: true,
        autoplaySpeed: 2500,
        adaptiveHeight: true,
        nextArrow: <ArrowDisplayNone/>,
        prevArrow: <ArrowDisplayNone/>,

    };
    return (

        <Slider
            {...settings}>
            {images.map(img => (
                <Grid
                    key={img.id}>
                    <img
                        className={classes.image}
                        src={img.image}
                        alt={img.title}/>
                    <div
                        style={{minHeight: '100px'}}
                    >
                        <div
                            className={classes.title}>
                            {img.title}
                        </div>
                        <div
                            className={classes.description}>
                            {img.description}
                        </div>
                    </div>

                </Grid>
            ))}
        </Slider>
    );
}

